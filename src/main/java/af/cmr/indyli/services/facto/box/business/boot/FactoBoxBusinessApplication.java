package af.cmr.indyli.services.facto.box.business.boot;

import af.cmr.indyli.services.facto.box.business.config.FactoBoxBusinessConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
@Import(FactoBoxBusinessConfig.class)
public class FactoBoxBusinessApplication {

    public static void main(String[] args) {
        SpringApplication.run(FactoBoxBusinessApplication.class, args);
/*
        String plainPassword = "password123";

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(plainPassword);

        System.out.println("++++++++++++++++++++++++++++++++" + encodedPassword);*/

    }

}
