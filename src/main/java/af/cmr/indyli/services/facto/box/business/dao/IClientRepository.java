package af.cmr.indyli.services.facto.box.business.dao;

import af.cmr.indyli.services.facto.box.business.entities.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IClientRepository extends JpaRepository<Client, Long> {
}
