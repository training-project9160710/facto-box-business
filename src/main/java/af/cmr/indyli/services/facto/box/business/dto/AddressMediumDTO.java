package af.cmr.indyli.services.facto.box.business.dto;

public class AddressMediumDTO extends AddressBasicDTO {

    private ClientBasicDTO clientBasicDTO;

    public AddressMediumDTO() {
    }

    public ClientBasicDTO getClientBasicDTO() {
        return clientBasicDTO;
    }

    public void setClientBasicDTO(ClientBasicDTO clientBasicDTO) {
        this.clientBasicDTO = clientBasicDTO;
    }
}
