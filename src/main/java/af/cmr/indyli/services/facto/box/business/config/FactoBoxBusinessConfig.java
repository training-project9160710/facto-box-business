package af.cmr.indyli.services.facto.box.business.config;

import org.modelmapper.ModelMapper;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EntityScan(basePackages = {"af.cmr.indyli.services.facto.box.business.entities"})
@ComponentScan(basePackages = {"af.cmr.indyli.services.facto.box.business.*"})
@EnableJpaRepositories(basePackages = {"af.cmr.indyli.services.facto.box.business.dao"})
public class FactoBoxBusinessConfig {

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
}
