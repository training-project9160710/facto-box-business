package af.cmr.indyli.services.facto.box.business.services.impl;

import af.cmr.indyli.services.facto.box.business.dao.IClientRepository;
import af.cmr.indyli.services.facto.box.business.dto.ClientBasicDTO;
import af.cmr.indyli.services.facto.box.business.dto.ClientFullDTO;
import af.cmr.indyli.services.facto.box.business.entities.Client;
import af.cmr.indyli.services.facto.box.business.services.IClientService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service(value = "clientService")
public class ClientServiceImpl implements IClientService {

    private final IClientRepository clientRepository;

    private final ModelMapper modelMapper;

    public ClientServiceImpl(IClientRepository clientRepository, ModelMapper modelMapper) {
        this.clientRepository = clientRepository;
        this.modelMapper = modelMapper;
    }


    @Override
    public ClientFullDTO createClient(ClientFullDTO clientFullDTO) {
        Client client = this.modelMapper.map(clientFullDTO, Client.class);
        client = this.clientRepository.save(client);
        clientFullDTO.setId(client.getId());
        return clientFullDTO;
    }

    @Override
    public ClientFullDTO getClientById(Long id) {
        Client client = this.clientRepository.findById(id).orElse(null);
        return this.modelMapper.map(client, ClientFullDTO.class);
    }

    @Override
    public ClientFullDTO updateClient(ClientFullDTO clientFullDTO) {
        return null;
    }

    @Override
    public ClientBasicDTO getAllClient() {
        return null;
    }
}
