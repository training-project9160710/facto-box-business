package af.cmr.indyli.services.facto.box.business.services;

import af.cmr.indyli.services.facto.box.business.dto.ClientBasicDTO;
import af.cmr.indyli.services.facto.box.business.dto.ClientFullDTO;

public interface IClientService {
    ClientFullDTO createClient(ClientFullDTO clientFullDTO);
    ClientFullDTO getClientById(Long id);
    ClientFullDTO updateClient(ClientFullDTO clientFullDTO);
    ClientBasicDTO getAllClient();

}
