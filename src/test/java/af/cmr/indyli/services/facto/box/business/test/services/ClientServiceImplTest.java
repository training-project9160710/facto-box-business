package af.cmr.indyli.services.facto.box.business.test.services;

import af.cmr.indyli.services.facto.box.business.dao.IClientRepository;
import af.cmr.indyli.services.facto.box.business.dto.ClientFullDTO;
import af.cmr.indyli.services.facto.box.business.entities.Client;
import af.cmr.indyli.services.facto.box.business.services.IClientService;
import af.cmr.indyli.services.facto.box.business.services.impl.ClientServiceImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;

import static org.junit.jupiter.api.Assertions.*;

class ClientServiceImplTest {
    IClientRepository clientRepository = Mockito.mock(IClientRepository.class);
    ModelMapper modelMapper = new ModelMapper();

    IClientService clientService = Mockito.mock(IClientService.class);

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    public void testCreateClient() {
        // Arrange
        ClientFullDTO clientFullDTO = new ClientFullDTO();
        clientFullDTO.setId(1L);
        clientFullDTO.setFirstName("John");
        clientFullDTO.setLastName("Doe");
        clientFullDTO.setEmail("john@example.com");


        // Mock
        Client savedClient = modelMapper.map(clientFullDTO, Client.class);
        Mockito.when(clientRepository.save(savedClient)).thenReturn(savedClient);
        Mockito.when(clientService.createClient(clientFullDTO)).thenReturn(clientFullDTO);

        // Act
        ClientFullDTO result = clientService.createClient(clientFullDTO);

        // Assert
        assertNotNull(result);
        assertEquals(1L, result.getId());
        assertEquals("John", result.getFirstName());
        assertEquals("Doe", result.getLastName());

    }


    @Test
    void getClientById() {
        // Arrange
        Long idClientFound = 1L;
        ClientFullDTO clientFullDTO = new ClientFullDTO();
        clientFullDTO.setId(idClientFound);

        // Mock
        Mockito.when(clientService.getClientById(idClientFound)).thenReturn(clientFullDTO);

        // Act
        ClientFullDTO result = clientService.getClientById(idClientFound);

        // Assert
        assertNotNull(result);
        assertEquals(1L, result.getId());
    }

    @Test
    void updateClient() {
    }

    @Test
    void getAllClient() {
    }
}
